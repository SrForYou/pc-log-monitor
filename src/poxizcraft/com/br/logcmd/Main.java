package poxizcraft.com.br.logcmd;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	
	public void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		
	}
	
	public void LogToFile(String Mensagem) {
		
        try
        
        {
            File DataFolder = getDataFolder();
            if(!DataFolder.exists())
            {
                DataFolder.mkdir();
            }
 
            File Salvar = new File(getDataFolder(), "log.txt");
            if (!Salvar.exists())
            {
                Salvar.createNewFile();
            }
 
 
            FileWriter Fw = new FileWriter(Salvar, true);
 
            PrintWriter Pw = new PrintWriter(Fw);
 
            Pw.println(Mensagem);
 
            Pw.flush();
 
            Pw.close();
 
        } catch (IOException e)
        {
 
            e.printStackTrace();
 
        }
 
    }
	
	@EventHandler
	public void QuandoUsarOComando(PlayerCommandPreprocessEvent e) {
		if(e.getPlayer().hasPermission("Monitorar.Log")){
			
			Date Data = new Date();
			SimpleDateFormat Formato = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			
			if(e.getMessage().contains("Login")){
				LogToFile ("[" + Formato.format(Data) + "] " + e.getPlayer().getName() + " Digitou O Comando: /login");
				return;
			}
			
			if(e.getMessage().contains("Register")){
				LogToFile ("[" + Formato.format(Data) + "] " + e.getPlayer().getName() + " Digitou O Comando: /register");
				return;
			}
			
			if(e.getMessage().contains("Changepassword")){
				LogToFile ("[" + Formato.format(Data) + "] " + e.getPlayer().getName() + " Digitou O Comando: /changepassword");
				return;
			}
			
			LogToFile ("[" + Formato.format(Data) + "] " + e.getPlayer().getName() + " Digitou O Comando: " + e.getMessage() );
		}
	}
	
	@EventHandler
	public void QuandoFalarNoChat(AsyncPlayerChatEvent e) {
		if(e.getPlayer().hasPermission("Monitorar.Log")){
			
			Date Data = new Date();
			SimpleDateFormat Formato = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			
			if(e.getMessage().contains("Login")){
				LogToFile ("[" + Formato.format(Data) + "] " + e.getPlayer().getName() + " Digitou O Comando: /login");
				return;
			}
			
			if(e.getMessage().contains("Register")){
				LogToFile ("[" + Formato.format(Data) + "] " + e.getPlayer().getName() + " Digitou O Comando: /register");
				return;
			}
			
			if(e.getMessage().contains("Changepassword")){
				LogToFile ("[" + Formato.format(Data) + "] " + e.getPlayer().getName() + " Digitou O Comando: /changepassword");
				return;
			}
			
			LogToFile ("[" + Formato.format(Data) + "] " + e.getPlayer().getName() + " Falou No Chat: " + e.getMessage() );
		}
	}
	
}
